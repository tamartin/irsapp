// Using HTMLUnit 2.17 library

// This program retrieves information from the IRS website regarding tax return preparers in a given zip code.
// The user is prompted to provide a zip and and their preferred sorting method (by last name or phone number).
// The resulting list is printed to the console.

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Vector;

import javax.swing.*;

import org.apache.commons.logging.LogFactory;

import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.*;

public class IRSApp {
	
	// Turn off HtmlUnit logging
	static {
        LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
    }
	// The URL for the IRS website
	String url = "http://www.irs.gov/uac/Authorized-IRS-e-file-Providers-for-Individuals";
	// Width of the GUI
	int width = 300;
	// Height of the GUI
	int height = 120;
	// Variable to store the zip code that was input by the user
	String zipCode = "";
	// Stores the sorting method that was input by the user
	boolean sortByName = true;
	
	// Create the components for the GUI
	JLabel prompt = new JLabel("Please enter your Zip Code:");
	JLabel prompt2 = new JLabel("Choose preferred sorting method:");
	JTextField input = new JTextField(5);
	JButton submit = new JButton("Submit");
	JRadioButton name = new JRadioButton("Last Name");
	JRadioButton number = new JRadioButton("Phone Number");
	ButtonGroup group = new ButtonGroup();
	Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	JPanel myPanel = new JPanel();
	JFrame myFrame = new JFrame("Retrieve Tax Return Preparers");
	
	// List to store the results from the IRS website
	List<Preparer> preparers = new Vector<Preparer>();
	
	// Handles all of the GUI initialization, interaction, and termination
	private void createGUI() {
		// Add an ActionListener to the submit button
		submit.addActionListener(new ActionListener() {
			// Create an ActionListener for the submit button
			public void actionPerformed(ActionEvent e)
            {
				// Retrieves what was typed in the text field
				String currentInput = input.getText();
				// Checks to make sure that a valid zip code was typed
                if(currentInput.length() != 5 || !currentInput.matches("[0-9]+")) System.out.println("Invalid input");
                else {
                	zipCode = currentInput;
                	// Determines which sorting method the user has chosen
                	if(name.isSelected()) sortByName = true;
                	else sortByName = false;
                	// Makes GUI invisible
                	myFrame.setVisible(false);
                	// Destroys GUI
            		myFrame.dispose();
                	}
                }
		});
		// Initialize the "name" RadioButton to be selected
		name.setSelected(true);
		// Add the two RadioButtons to a group 
		group.add(name);
		group.add(number);
		// Add all of the components to a panel
		myPanel.add(prompt);
		myPanel.add(input);
		myPanel.add(prompt2);
		myPanel.add(name);
		myPanel.add(number);
		myPanel.add(submit);
		// Set the GUI properties
		myFrame.setSize(width, height);
		myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myFrame.setLocation(dim.width/2-width/2, dim.height/2-height/2);
		myFrame.add(myPanel);
		myFrame.setVisible(true);
		
	}
	// Handles all interactions with the IRS website
	private void processHtml() throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		// Creates a WebCLient object from the HtmlUnit library
		WebClient wc = new WebClient();
		wc.getOptions().setCssEnabled(false);
		wc.getOptions().setJavaScriptEnabled(false);
		// Connects to the IRS website
		HtmlPage firstPage = wc.getPage(url);
		// Finds the form on the IRS website for inputting zip code
		HtmlForm form = firstPage.getFormByName("file");
		// Finds the submit button on the IRS website
		HtmlSubmitInput submitButton = (HtmlSubmitInput) form.getLastElementChild();
		// Finds the box for text input 
		HtmlTextInput zipText = form.getInputByName("q");
		// Sets the input to be the desired zip code that was set by the user
		zipText.setValueAttribute(zipCode);
		// Click the submit button and store the resulting page
		HtmlPage secondPage = submitButton.click();
		// Find how many results are listed
		List<DomElement> tables = secondPage.getElementsByTagName("table");
		// Store the number of results
		int numTables = tables.size();
		// Iterate through all of the tables
		for (int i=0; i < numTables; i++){
			final HtmlTable table = (HtmlTable) secondPage.getByXPath("//table").get(i);
			// Array to store each element of the table (i.e. Business name, address,
			// location, contact name, and phone number
			String[] data = new String[5];
			// Counter to keep track of which row of the table we are looking at
			int rowCount = 0;
			// Iterate through the rows of the table
	         for (final HtmlTableRow row : table.getRows()) {
	        	 // Counter to keep track of which column of the table we are looking at
	        	 int colCount = 0;
	        	 // Iterate through the columns
	                for (final HtmlTableCell cell : row.getCells()) {
	                	// Stores elements of the table into the data array
	                	if (colCount == 1) data[rowCount] = cell.asText();
	                	colCount++;
	                }
	                rowCount++;
	         }
	         // Adds results to the list of Preparers
	         preparers.add(new Preparer(data[0], data[1], data[2], data[3], data[4]));
		}
		wc.close();
	}
	
	public void sortPreparers(){
		if (sortByName) insertionSort("name");
		else insertionSort("number");
	}
	
	public void insertionSort(String s){
		for (int i=1; i<preparers.size(); i++){
			Preparer tempPreparer = preparers.get(i);
			int j = i;
			while(j > 0 && !preparers.get(j-1).compare(s, tempPreparer)){
					preparers.set(j, preparers.get(j-1));
					j--;
			}
			preparers.set(j, tempPreparer);
		}
	}
	
	public void printPreparers(){
		if(preparers.size()==0){
			System.out.println("There are no tax return preparers in your zip code.");
			return;
		}
		for(int i=0; i<preparers.size(); i++){
			Preparer cur = preparers.get(i);
			System.out.println(cur.getBusiness() + ", " + cur.getAddress() + ", "
					+ cur.getLocation() + ", " + cur.getContact() + ", " + cur.getPhone());
			System.out.println();
		}
	}
	
	public static void main (String[] args) throws InterruptedException, FailingHttpStatusCodeException, MalformedURLException, IOException {
		IRSApp app = new IRSApp();
		app.createGUI();
		// Wait for user input
		while(app.zipCode == "") Thread.sleep(200);
		app.processHtml();
		System.out.println("Results for the ZipCode: " + app.zipCode);
		System.out.println();
		app.sortPreparers();
		app.printPreparers();
	}
}
