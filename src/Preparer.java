
public class Preparer {

	private String business;
	private String address;
	private String location;
	private String contact;
	private String phone;
	private String alphabet = "'0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	public Preparer (String b, String a, String l, String c, String p) {
		business = b;
		address = a;
		location = l;
		contact = c;
		phone = p;
		
	}
	
	public String getBusiness(){
		return business;
	}
	
	public String getAddress(){
		return address;
	}
	
	public String getLocation(){
		return location;
	}
	
	public String getContact(){
		return contact;
	}
	
	public String getPhone(){
		return phone;
	}
	
	public boolean compare(String s, Preparer p){
		if(s == "name") return this.compareNames(p);
		else return compareNumbers(p);
	}
	
	// Compares the last name of this Preparer to the last name of the parameter Preparer
	// Returns true if this Preparer comes before the parameter Preparer alphabetically
	// Returns false is the parameter Preparer comes before this one
	public boolean compareNames(Preparer p){
		boolean done = false;
		boolean returnValue = false;
		int thisIndex = this.findLastName();
		int otherIndex = p.findLastName();
		while(!done){
			if(thisIndex == contact.length() || otherIndex == p.getContact().length()) return false;
			// Calculate the current character for each Preparer and convert it to uppercase
			// for comparison
			char thisChar = Character.toUpperCase(this.getContact().charAt(thisIndex));
			char otherChar = Character.toUpperCase(p.getContact().charAt(otherIndex));
			// If the characters are the same, move to the next character
			if(thisChar == otherChar){
				thisIndex++;
				otherIndex++;
			}
			else {
				// Determine which comes first alphabetically
				int thisScore = alphabet.indexOf(thisChar);
				int otherScore = alphabet.indexOf(otherChar);
				if(thisScore < otherScore) returnValue = true;
				else returnValue = false;
				done = true;
			}
		}
		return returnValue;
	}
	
	// Compares the phone number of this Preparer to the phone number of the parameter Preparer
	// Returns true if this Preparer's phone number comes before the parameter Preparer's phone number
	// Returns false if the parameter Preparer's phone number comes before this Preparer's phone number
	public boolean compareNumbers(Preparer p){
		boolean done = false;
		boolean returnValue = false;
		int index = 0;
		while(!done){
			if(index == phone.length() || index == p.phone.length()) return false;
			char thisChar = this.getPhone().charAt(index);
			char otherChar = p.getPhone().charAt(index);
			// If the characters are the same, move to the next character
			if(thisChar == otherChar) index++;
			else{
				// Determine which number is lower
				int thisIndex = alphabet.indexOf(thisChar);
				int otherIndex = alphabet.indexOf(otherChar);
				if(thisIndex < otherIndex) returnValue = true;
				else returnValue = false;
				done = true;
			}
		}
		return returnValue;
	}
	
	// Determines which character in this Preparers contact field marks the beginning
	// of the last name.  Returns the index of this character
	public int findLastName(){
		// Finds the index of the last blank space that occurs in the contact string
		int lastSpace = this.contact.lastIndexOf(" ");
		// Add 1 to the index to get where the last name starts
		return lastSpace + 1;
	}
}
